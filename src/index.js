import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'

import ArrowKeyNavigator from './ArrowKeyNavigator'

class Example extends Component {
  state = {
    currentFocus: null
  }
  handleFocus = (e) => {
    this.setState({ currentFocus: e.target })
  }
  render() {
    const { currentFocus } = this.state
    return (
      <form>
        <ArrowKeyNavigator currentFocus={currentFocus} nbColumns={4}>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
          <div className="row">
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
            <input type="text" className="input" onFocus={this.handleFocus} />
          </div>
        </ArrowKeyNavigator>
      </form>
    )
  }
}

ReactDOM.render(<Example />, document.getElementById('root'))
registerServiceWorker()
