import React, { Component } from 'react'
import PropTypes from 'prop-types'

const findInput = (list) => {
  return list.reduce((ac, v) => {
    if (v.nodeName !== 'INPUT') {
      return [...ac, ...findInput([...v.childNodes])]
    }
    return [...ac, v]
  }, [])
}

const move = (bool, selectedItem) => {
  if (bool) {
    if (selectedItem) selectedItem.focus()
  }
}

class ArrowKeyNavigator extends Component {
  state = {
    list: []
  }
  componentDidMount = () => {
    if (this.container) {
      const inputList = findInput([...this.container.childNodes])
      this.setState({ list: inputList })
    }
  }

  moveFocusUp = (index, columns, nbColumns) => {
    const { list } = this.state
    const isOnfirstLine = index < columns
    const selectedIndex = index - nbColumns

    move(!isOnfirstLine, list[selectedIndex])
  }

  moveFocusDown = (index, columns, nbColumns) => {
    const { list } = this.state
    const length = list.length - 1
    const isOnLastLine = index >= length - columns
    const selectedIndex = index + nbColumns

    move(!isOnLastLine, list[selectedIndex])
  }

  moveFocusLeft = (index, columns, nbColumns) => {
    const { list } = this.state
    const isOnFirstColumns = index % nbColumns === 0
    const selectedIndex = index - 1

    move(!isOnFirstColumns, list[selectedIndex])
  }

  moveFocusRight = (index, columns, nbColumns) => {
    const { list } = this.state
    const isOnFirstColumns = index % nbColumns - columns === 0
    const selectedIndex = index + 1

    move(!isOnFirstColumns, list[selectedIndex])
  }

  handleKeyDow = (e) => {
    const { currentFocus, nbColumns } = this.props
    const { list } = this.state
    const index = list.indexOf(currentFocus)
    const columns = nbColumns - 1
    if (currentFocus) {
      switch (e.key) {
        case 'ArrowUp':
          this.moveFocusUp(index, columns, nbColumns)
          return null
        case 'ArrowDown':
          this.moveFocusDown(index, columns, nbColumns)
          return null
        case 'ArrowRight':
          this.moveFocusRight(index, columns, nbColumns)
          return null
        case 'ArrowLeft':
          this.moveFocusLeft(index, columns, nbColumns)
          return null
        default:
          return null
      }
    }
  }

  render() {
    return (
      <div
        ref={(elm) => {
          this.container = elm
        }}
        className={this.props.className}
        style={this.props.style}
        onKeyDown={this.handleKeyDow}
      >
        {this.props.children}
      </div>
    )
  }
}

ArrowKeyNavigator.defaultProps = {
  className: '',
  style: {},
  currentFocus: null
}

ArrowKeyNavigator.propTypes = {
  children: PropTypes.node.isRequired,
  currentFocus: PropTypes.shape({}),
  className: PropTypes.string,
  style: PropTypes.shape({}),
  nbColumns: PropTypes.number.isRequired
}

export default ArrowKeyNavigator
